/* 04 - Curso SQL - Cláusula Order By - Ordenando registros */

SELECT * FROM productos WHERE seccion = 'DEPORTES' OR seccion = 'CERÁMICA' ORDER BY seccion ASC ;  #ASCENDENTE
SELECT * FROM productos WHERE seccion = 'DEPORTES' OR seccion = 'CERÁMICA' ORDER BY seccion DESC; #DESCENDENTE

SELECT * FROM productos WHERE seccion = 'DEPORTES' OR seccion = 'CERÁMICA' ORDER BY precio ASC;  #ASCENDENTE
SELECT * FROM productos WHERE seccion = 'DEPORTES' OR seccion = 'CERÁMICA' ORDER BY precio DESC; #DESCENDENTE


SELECT * FROM productos WHERE seccion = 'DEPORTES' OR seccion = 'CERÁMICA' ORDER BY seccion ASC, precio ASC;
SELECT * FROM productos WHERE seccion = 'DEPORTES' OR seccion = 'CERÁMICA' ORDER BY seccion ASC, precio DESC;

SELECT * FROM productos WHERE seccion = 'DEPORTES' OR seccion = 'CERÁMICA' ORDER BY seccion ASC, paisOrigen ASC;
SELECT * FROM productos WHERE seccion = 'DEPORTES' OR seccion = 'CERÁMICA' ORDER BY seccion ASC, paisOrigen ASC, precio ASC;
