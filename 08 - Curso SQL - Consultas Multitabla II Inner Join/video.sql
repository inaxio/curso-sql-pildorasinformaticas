/* 08 - Curso SQL - Consultas Multitabla II Inner Join */

SELECT * FROM clientes
  INNER JOIN pedidos ON clientes.codigoCliente = pedidos.codigoCliente
  WHERE poblacion = 'MADRID';
