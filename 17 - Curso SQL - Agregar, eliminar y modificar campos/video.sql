/* 17 - Curso SQL - Agregar, eliminar y modificar campos */

# AGREGAR UN CAMPO A UNA TABLA
ALTER TABLE clientes_madrid ADD COLUMN fechaBaja DATE;
ALTER TABLE clientes_madrid ADD COLUMN fechaBaja VARCHAR(20);
ALTER TABLE prueba  ADD COLUMN poblacion VARCHAR(20);
ALTER TABLE prueba  ADD COLUMN lugarNacimiento VARCHAR(20);

# BORRAR UN CAMPO DE UNA TABLA
ALTER TABLE prueba DROP COLUMN poblacion;
ALTER TABLE clientes_madrid DROP COLUMN fechaBaja;

# MODIFICAR UN CAMPO DE UNA TABLA
# Access
ALTER TABLE clientes_madrid ALTER COLUMN fechaBaja DATE;
ALTER TABLE clientes_madrid ALTER COLUMN fechaBaja VARCHAR(20);
# MySQL
ALTER TABLE clientes_madrid CHANGE fechaBaja fechaBaja DATE;
ALTER TABLE clientes_madrid CHANGE fechaBaja fechaBaja VARCHAR(20);
  ## Poner valor por defecto
ALTER TABLE clientes_madrid ALTER COLUMN fechaBaja SET DEFAULT 'Valor por defecto'; # PARA VARCHAR(20)
ALTER TABLE clientes_madrid ALTER COLUMN fechaBaja SET DEFAULT '2016-12-31';        # PARA DATE
ALTER TABLE prueba ALTER COLUMN lugarNacimiento SET DEFAULT 'Desconocido';
  ## Quitar valor por defecto
ALTER TABLE prueba ALTER COLUMN lugarNacimiento DROP DEFAULT;


