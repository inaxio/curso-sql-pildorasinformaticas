/* 16 - Curso SQL - DDL Creación de tablas */

# BORRAR UNA TABLA
DROP TABLE prueba;

########################################################
# CREAR UNA TABLA BÁSICA CON UN CAMPO
# MySQL
CREATE TABLE prueba (nombre varchar(60));
# Access
CREATE TABLE prueba (nombre text(60));

########################################################
# CREAR UNA TABLA CON VARIOS CAMPOS
# MySQL
CREATE TABLE prueba (
  nombre VARCHAR(40),
  apellido VARCHAR(40),
  edad TINYINT,
  fechaNacimiento DATE,
  carnet BOOL
);
# Access
CREATE TABLE prueba (
  nombre TEXT(40),
  apellido TEXT(40),
  edad BYTE,
  fechaNacimiento DATE,
  carnet BIT
);

########################################################
# CREAR UNA TABLA CON CAMPO AUTO-NUMÉRICO
# MySQL (Requiere de clave primaria)
CREATE TABLE prueba (
  idAlumno INT AUTO_INCREMENT,
  nombre VARCHAR(40),
  apellido VARCHAR(40),
  edad TINYINT,
  fechaNacimiento DATE,
  carnet BOOL,
  PRIMARY KEY (idAlumno)
);
# Access
CREATE TABLE prueba (
  idAlumno COUNTER,
  nombre TEXT(40),
  apellido TEXT(40),
  edad BYTE,
  fechaNacimiento DATE,
  carnet BIT
);
