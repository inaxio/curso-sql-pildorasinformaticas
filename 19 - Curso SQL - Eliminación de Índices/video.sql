/* 19 - Curso SQL - Eliminación de Índices */

# Crar tabla Access
CREATE TABLE ejemplo (
  dni TEXT,
  nombre TEXT,
  apellido TEXT,
  edad NUMBER
);

# Crar tabla MySQL
CREATE TABLE ejemplo (
  dni VARCHAR(20),
  nombre VARCHAR(20),
  apellido VARCHAR(20),
  edad TINYINT
);

# Crear índice
CREATE INDEX miIndice ON ejemplo (dni);

# Borrar índice
DROP INDEX miIndice ON ejemplo;

# Añadir clave primaria
ALTER TABLE ejemplo ADD PRIMARY KEY (dni);

# Borrar clave primaria
# Access / SQL Server / Oracle
ALTER TABLE ejemplo DROP CONSTRAINT Index_BBC2FF62_AFDA_4D49;
# MySQL
ALTER TABLE ejemplo DROP PRIMARY KEY;



