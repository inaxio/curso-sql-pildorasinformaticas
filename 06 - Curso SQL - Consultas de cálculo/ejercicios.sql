/* 06 - Curso SQL - Consultas de cálculo */

# 1. Realizar una consulta que visualice los campos NOMBRE ARTÍCULO, SECCIÓN, PRECIO de la tabla PRODUCTOS
#    y un campo nuevo que nombramos con el texto “DESCUENTO_7”.
#    Debe mostrar el resultado de aplicar sobre el campo PRECIO un descuento de un 7 %.
#    El formato del nuevo campo para debe aparecer con 2 lugares decimales.
SELECT nombreArticulo, seccion, precio, ROUND(precio*0.93, 2) AS  DESCUENTO_7 FROM productos;
SELECT nombreArticulo, seccion, precio, ROUND(precio-(precio*0.07), 2) AS  DESCUENTO_7 FROM productos;

# 2. Realizar una consulta visualizando los campos FECHA, SECCIÓN, NOMBRE ARTÍCULO
#    y PRECIO de la tabla PRODUCTOS y un campo nuevo que nombramos con el texto “DTO2 €_EN_CERÁMICA”.
#    Debe mostrar el resultado de aplicar sobre el campo PRECIO la resta de 2 € sólo a los artículos de la sección CERÁMICA.
#    El formato del nuevo campo debe aparecer con 2 lugares decimales.
#    Ordenar el resultado de la consulta por el campo FECHA descendente.
SELECT fecha, date_format(fecha,'%d-%m-%Y') AS FechaES, seccion, nombreArticulo, precio, round(precio-2, 2) AS DTO2_€_EN_CERÁMICA FROM productos WHERE seccion = 'CERÁMICA' ORDER BY fecha ASC;

# 3. Realizar una consulta visualizando los campos NOMBRE ARTÍCULO, SECCIÓN, PRECIO de la tabla PRODUCTOS
#    y un campo nuevo que nombramos con el texto “PRECIO_AUMENTADO_EN_2”.
#    Debe mostrar el PRECIO con un incremento de un 2% del PRECIO. Sólo debemos tener en cuenta los registros de la sección FERRETERÍA.
#    El nuevo campo debe aparecer en Euros y con 2 lugares decimales.
SELECT nombreArticulo seccion, precio, ROUND(precio*1.02, 2) as PRECIO_AUMENTADO_EN_2 FROM productos WHERE seccion = 'FERRETERÍA'
SELECT nombreArticulo seccion, precio, CONCAT(ROUND(precio*1.02, 2), ' €') as PRECIO_AUMENTADO_EN_2 FROM productos WHERE seccion = 'FERRETERÍA'