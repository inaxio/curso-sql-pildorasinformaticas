/* 06 - Curso SQL - Consultas de cálculo */

SELECT  nombreArticulo,
        seccion, ROUND(precio, 2)  AS `Base Imponible`,
        ROUND(precio*0.21, 2) AS IVA,
        ROUND(precio*1.21, 2) AS `PVP`
  FROM productos;

SELECT  nombreArticulo,
        seccion, ROUND(precio, 2)  AS `Precio`,
        round(precio-3, 2) AS `Precio con Descuento`
  FROM productos;

SELECT  nombreArticulo, seccion, precio,
        fecha, NOW() AS Ahora,
        datediff(now(),fecha) AS `Días de diferencia`
  FROM productos
  WHERE seccion = 'DEPORTES';

SELECT  nombreArticulo, seccion, precio, fecha,
        DATE_FORMAT(NOW(),'%d-%m-%Y') AS Ahora,
        datediff(now(),fecha) AS `Días de diferencia`
  FROM productos WHERE seccion = 'DEPORTES';