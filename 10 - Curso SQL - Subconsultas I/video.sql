/* 10 - Curso SQL - Subconsultas I */

# POR ENCIMA DE LA MEDIA
SELECT codigoArticulo, seccion, nombreArticulo, round(precio, 2) FROM productos
  WHERE precio > ( SELECT AVG(precio) FROM productos );

# POR DEBAJO DE LA MEDIA
SELECT codigoArticulo, seccion, nombreArticulo, round(precio, 2) FROM productos
  WHERE precio < ( SELECT AVG(precio) FROM productos );

###############################################################################

SELECT nombreArticulo, seccion, precio FROM productos
  WHERE precio > ( SELECT MAX(precio) FROM productos WHERE seccion = 'CERÁMICA' )
  ORDER BY precio;

# ALL ( Mayor que Todos )
SELECT nombreArticulo, seccion, precio FROM productos
  WHERE precio > ALL ( SELECT precio FROM productos WHERE seccion = 'CERÁMICA' )
  ORDER BY precio;

# ANY ( Mayor que Cualquiera )
SELECT nombreArticulo, seccion, precio FROM productos
  WHERE precio > ANY ( SELECT precio FROM productos WHERE seccion = 'CERÁMICA' )
  ORDER BY precio;

###############################################################################

# ALL ( Mayor que Todos )
SELECT * FROM productos WHERE precio > ALL ( SELECT precio FROM productos  WHERE seccion = 'JUGUETERÍA' );
# ANY ( Mayor que Cualquiera )
SELECT * FROM productos WHERE precio > ANY ( SELECT precio FROM productos  WHERE seccion = 'JUGUETERÍA' );
