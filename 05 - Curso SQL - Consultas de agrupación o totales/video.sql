/* 05 - Curso SQL - Consultas de agrupación o totales */

SELECT seccion, sum(precio)
  FROM productos
  GROUP BY seccion
  ORDER BY sum(precio) ASC;

SELECT seccion, sum(precio) AS `Suma Artículos`
  FROM productos
  GROUP BY seccion
  ORDER BY `Suma Artículos` ASC;

SELECT seccion, avg(precio) AS `Media Deportes y Confección`
  FROM productos WHERE seccion = 'DEPORTES' OR seccion = 'CONFECCIÓN'
  GROUP BY seccion
  ORDER BY `Media Deportes y Confección` ASC;

SELECT seccion, avg(precio) AS `Media Deportes y Confección`
  FROM productos
  GROUP BY seccion  HAVING seccion = 'DEPORTES' OR seccion = 'CONFECCIÓN'
  ORDER BY `Media Deportes y Confección` ASC;

SELECT poblacion, count(codigoCliente) as `Número de clientes`
  FROM clientes
  GROUP BY poblacion;

SELECT seccion, max(precio) AS `Precio mas alto`
  FROM productos
  WHERE seccion = 'CONFECCIÓN'
  GROUP BY seccion;