/* 05 - Curso SQL - Consultas de agrupación o totales */

# 1.	Realizar una consulta sobre la tabla “Clientes”  que muestre los campos “Dirección”, “Teléfono” y “Población”.
#     Este último debe aparecer en la consulta con el nombre de “Residencia”. Los registros aparecerán ordenados descendentemente por el campo “Población”.
SELECT direccion, telefono, poblacion AS Residencia FROM clientes ORDER BY poblacion;
SELECT direccion, telefono, poblacion AS Residencia FROM clientes ORDER BY Residencia;

# 2.	Realizar una consulta que muestre que poblaciones hay en la tabla “Clientes”.
SELECT poblacion FROM clientes GROUP BY poblacion ORDER BY poblacion;
SELECT DISTINCT poblacion FROM clientes ORDER BY poblacion;

# 3.	Realizar una consulta de agrupación que muestre la media del precio de los artículos de todas las secciones.
#     Mostrar en la consulta los campos sección y suma por sección.
SELECT avg(precio) Media FROM productos;
SELECT seccion, SUM(precio) as `Suma por Sección` FROM productos GROUP BY seccion ORDER BY `Suma por Sección` ASC ;

# 4.	Realizar una consulta de agrupación que muestre la media del precio de todas las secciones menos de juguetería.
#     En la consulta deberán aparecer los campos “Sección” y “Media por sección”.
SELECT seccion AS Sección, avg(precio) AS  `Media por sección` FROM productos WHERE seccion <> 'JUGUETERIA' GROUP BY seccion;
SELECT seccion AS Sección, avg(precio) AS  `Media por sección` FROM productos WHERE seccion NOT LIKE 'JUGUETERIA' GROUP BY seccion;

# 5.	Realizar Una consulta que muestre cuantos artículos hay de la sección “Deportes”.
SELECT seccion, count(codigoArticulo) as Cantidad FROM productos WHERE seccion = 'DEPORTES';
SELECT seccion, count(codigoArticulo) as Cantidad FROM productos WHERE seccion LIKE 'DEPORTES';
SELECT seccion, count(*) as Cantidad FROM productos WHERE seccion LIKE 'DEPORTES';
SELECT seccion, count(seccion) as Cantidad FROM productos WHERE seccion LIKE 'DEPORTES';