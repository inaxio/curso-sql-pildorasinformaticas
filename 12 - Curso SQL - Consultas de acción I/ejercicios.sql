/* 12 - Curso SQL - Consultas de acción I */

# 1. Realizar una consulta de acción de creación de tabla a partir de la tabla CLIENTES,
#    utilizando todos los campos de la tabla, pero únicamente los registros que sean de la población Madrid.
#    El nuevo objeto lo nombramos con el texto “CLIENTES_DE_MADRID”.
#    Ejecutamos la consulta.
# Access
SELECT * INTO clientes_madrid FROM clientes WHERE poblacion = 'MADRID';
# MySQL
CREATE TABLE clientes_madrid SELECT * FROM clientes WHERE poblacion = 'MADRID';


# 2. Realizar una consulta de acción de creación de tabla a partir de la tabla PRODUCTOS,
#    utilizando todos los campos de la tabla, pero sólo los registros que sean de la sección DEPORTES.
#    El nuevo objeto – tabla lo nombramos con el texto "ARTICULOS_DE_DEPORTES".
#    Ejecutamos la consulta.
# Access
SELECT * INTO articulos_de_deportes FROM productos WHERE seccion = 'DEPORTES';
# MySQL
CREATE TABLE articulos_de_deportes SELECT * FROM productos WHERE seccion = 'DEPORTES';


# 3. Realizar una consulta de acción de creación de tabla a partir de la tabla PEDIDOS,
#    utilizando todos los campos de la tabla, pero sólo los registros que tengan registrada la forma de pago TARJETA.
#    El nuevo objeto – tabla lo nombramos con el texto "PEDIDOS_PAGADOS_CON TARJETA".
#    Ejecutamos la consulta.
# Access
SELECT * INTO pedidos_pagados_con_tarjeta FROM pedidos WHERE formaPago = 'TARJETA';
# MySQL
CREATE TABLE pedidos_pagados_con_tarjeta SELECT * FROM pedidos WHERE formaPago = 'TARJETA';

# 4. Realizar una consulta que actualice los precios de la tabla "ARTICULOS_DE_DEPORTES".
#    La actualización consiste en calcular el IVA (21%) y mostrar en ese campo como resultado el precio con el IVA incluido.
#    Ejecutar la consulta.
UPDATE articulos_de_deportes SET precio = precio*1.21 WHERE seccion = 'DEPORTES';


# 5. Realizar una consulta que actualice el campo DESCUENTO de la tabla "PEDIDOS_PAGADOS_CON TARJETA".
#    La actualización consiste poner a un 5% los descuentos que se muestran inferiores a esta cifra.
#    Ejecutar la consulta.
UPDATE pedidos_pagados_con_tarjeta SET descuento = 0.05 WHERE descuento < 0.05;