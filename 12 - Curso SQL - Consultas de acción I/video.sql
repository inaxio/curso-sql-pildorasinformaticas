/* 12 - Curso SQL - Consultas de acción I */

# CONSULTAS DE ACTUALIZACIÓN
UPDATE productos SET precio = precio+10 WHERE seccion = 'DEPORTE';
UPDATE productos SET precio = precio-10 WHERE seccion = 'DEPORTE';

UPDATE productos SET seccion = 'DEPORTIVOS' WHERE seccion = 'DEPORTES';
UPDATE productos SET seccion = 'DEPORTES' WHERE seccion = 'DEPORTIVOS';

# CREACION DE TABLA
# Access
SELECT * INTO clientes_madrid FROM clientes WHERE poblacion = 'MADRID';
#MySQL
CREATE TABLE clientes_madrid SELECT * FROM clientes WHERE poblacion = 'MADRID';

