/* 25 - Curso SQL - Vistas - Final curso */

# Borrar vista
DROP VIEW IF EXISTS vistaArticulosDeportes;

# Crear vista
CREATE VIEW vistaArticulosDeportes AS
  SELECT nombreArticulo, seccion, precio
  FROM productos
  WHERE seccion = 'DEPORTES'
  ORDER BY precio;

# Consultar vista
SELECT * FROM vistaArticulosDeportes;

# Consulta de actualización
UPDATE productos SET precio = precio+10
WHERE nombreArticulo = 'RAQUETA TENIS';

# Borrar vista
DROP VIEW IF EXISTS vistaArticulosCeramica;

# Crear vista
CREATE VIEW vistaArticulosCeramica AS
  SELECT nombreArticulo, seccion, precio
  FROM productos
  WHERE seccion = 'CERÁMICA'
  ORDER BY precio;

# Consultar vista
SELECT * FROM vistaArticulosCeramica;