/* 07 - Curso SQL - Consultas Multitabla I */

select * FROM productos where seccion = 'DEPORTES' UNION SELECT * FROM productosnuevos WHERE seccion = 'DEPORTES DE RIESGO';

select * FROM productos where precio > 500 UNION SELECT * FROM productosnuevos WHERE seccion = 'ALTA COSTURA';

# Código para insertar el articulo.
# INSERT INTO productosnuevos (codigoArticulo, seccion, nombreArticulo, precio, fecha, importado, paisOrigen, foto) VALUES ('AR06', 'DEPORTES', 'MANCUERNAS', '60.0000', '2000-09-13', 'VERDADERO', 'USA', NULL);

# con UNION
select * FROM productos where seccion = 'DEPORTES' UNION select * FROM productosnuevos where seccion = 'DEPORTES' ORDER BY codigoArticulo ASC;
# con UNION ALL
select * FROM productos where seccion = 'DEPORTES' UNION ALL select * FROM productosnuevos where seccion = 'DEPORTES' ORDER BY codigoArticulo ASC;