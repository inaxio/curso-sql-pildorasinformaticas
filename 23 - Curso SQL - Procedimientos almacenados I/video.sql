/* 23 - Curso SQL - Procedimientos almacenados I */

# Crear procedimiento simple
CREATE PROCEDURE muestraClientes()
  SELECT * FROM clientes WHERE poblacion = 'MADRID';

# Llamar al procedimiento
CALL muestraClientes();

# Crear procedimiento con parámetro
CREATE PROCEDURE actulizaProductos( nuevoPrecio INT, codigo VARCHAR(4) )
  UPDATE productos SET precio = nuevoPrecio WHERE codigoArticulo = codigo;

# Llamar al procedimiento
CALL actulizaProductos(425,'AR28');
CALL actulizaProductos(60,'AR22');