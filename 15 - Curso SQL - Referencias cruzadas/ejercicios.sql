/* 15 - Curso SQL - Referencias cruzadas */

# 1. Realiza una consulta de referencias cruzadas
#    que muestre cuántos artículos hay en la tabla de productos por cada año.
TRANSFORM COUNT(CÓDIGOARTÍCULO) AS CANTIDAD
SELECT PAÍSDEORIGEN, COUNT(CÓDIGOARTÍCULO) AS TOTAL
FROM PRODUCTOS
GROUP BY PAÍSDEORIGEN
PIVOT FORMAT(FECHA,"yyyy");

# 2. Realiza una consulta de referencias cruzadas que muestre cuántos artículos importados
#    y no importados han sido enviados, y cuántos importados y no importados hay sin enviar.
# CONSULTA PREVIA2
SELECT PRODUCTOS.CÓDIGOARTÍCULO, IMPORTADO, ENVIADO
FROM PRODUCTOS INNER JOIN
  ( PEDIDOS INNER JOIN PRODUCTOSPEDIDOS ON PEDIDOS.NÚMERODEPEDIDO = PRODUCTOSPEDIDOS.NÚMERODEPEDIDO)
  ON PRODUCTOS.CÓDIGOARTÍCULO = PRODUCTOSPEDIDOS.CÓDIGOARTÍCULO;
# CONSULTA REFERENCIAS CRUZADAS
TRANSFORM COUNT(CÓDIGOARTÍCULO) AS CuentaDeCódigoArticulo
SELECT IMPORTADO
FROM PREVIA2
GROUP BY IMPORTADO
PIVOT ENVIADO;
