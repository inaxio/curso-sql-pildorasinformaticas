/* 14 - Curso SQL - Consultas de acción III - Consultas de datos anexados */

INSERT INTO clientes SELECT * FROM clientes_madrid;

DELETE FROM clientes WHERE poblacion = 'MADRID';


INSERT INTO clientes(codigoCliente, empresa, poblacion, telefono)
  SELECT codigoCliente, empresa, poblacion, telefono FROM clientes_madrid;

# Da error por falta de el campo clave.
INSERT INTO clientes( empresa, poblacion, telefono)
  SELECT empresa, poblacion, telefono FROM clientes_madrid;
