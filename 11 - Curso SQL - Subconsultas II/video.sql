/* 11 - Curso SQL - Subconsultas II */

# CON LISTA
SELECT nombreArticulo, precio
  FROM productos
  WHERE codigoArticulo IN ( SELECT codigoArticulo FROM productospedidos WHERE unidades > 20 )
  ORDER BY nombreArticulo;

# CON INNER JOIN
SELECT nombreArticulo, precio
  FROM productos INNER JOIN productospedidos ON productos.codigoArticulo = productospedidos.codigoArticulo
  WHERE unidades > 20
  ORDER BY nombreArticulo;

# NOT IN
SELECT empresa, poblacion
  FROM clientes
  WHERE codigoCliente NOT IN ( SELECT DISTINCT codigoCliente FROM pedidos WHERE formaPago = 'TARJETA' );
# SELECT DISTINCT para evitar repeticiones