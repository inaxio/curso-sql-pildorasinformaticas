/* 20 - Curso SQL - Triggers I Disparadores */

# Crear tabla regProductos
CREATE TABLE regProductos(
  codigoArticulo VARCHAR(25),
  nombreArticulo VARCHAR(30),
  precio int(4),
  insertado DATETIME
);

# Crear trigger
CREATE TRIGGER productos_ai
AFTER INSERT ON productos
FOR EACH ROW
  INSERT INTO regProductos(codigoArticulo, nombreArticulo, precio, insertado)
      VALUES (NEW.codigoArticulo, NEW.nombreArticulo, new.precio, NOW() );

# Insertar un producto
INSERT INTO productos (codigoArticulo, nombreArticulo, precio, paisOrigen)
VALUES ('AR75', 'Pantalón', 50, 'ESPAÑA');
