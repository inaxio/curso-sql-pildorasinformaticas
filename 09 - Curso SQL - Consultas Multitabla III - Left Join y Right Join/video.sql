/* 09 - Curso SQL - Consultas Multitabla III - Left Join y Right Join */

# INNER JOIN
SELECT * FROM clientes
  INNER JOIN pedidos ON clientes.codigoCliente = pedidos.codigoCliente
  WHERE poblacion = 'MADRID'
  ORDER BY clientes.codigoCliente ASC , numeroPedido ASC;
# LEFT JOIN
SELECT * FROM clientes
  LEFT JOIN pedidos ON clientes.codigoCliente = pedidos.codigoCliente
  WHERE poblacion = 'MADRID'
  ORDER BY clientes.codigoCliente ASC , numeroPedido ASC;
# RIGHT JOIN
SELECT * FROM clientes
  RIGHT JOIN pedidos ON clientes.codigoCliente = pedidos.codigoCliente
  WHERE poblacion = 'MADRID'
  ORDER BY clientes.codigoCliente ASC , numeroPedido ASC;

# INNER JOIN
SELECT clientes.codigoCliente, poblacion, direccion, numeroPedido, pedidos.codigoCliente, formaPago
  FROM clientes INNER JOIN pedidos ON clientes.codigoCliente = pedidos.codigoCliente
  WHERE poblacion = 'MADRID';
# LEFT LEFT
SELECT clientes.codigoCliente, poblacion, direccion, numeroPedido, pedidos.codigoCliente, formaPago
  FROM clientes LEFT JOIN pedidos ON clientes.codigoCliente = pedidos.codigoCliente
  WHERE poblacion = 'MADRID';
# LEFT RIGHT
SELECT clientes.codigoCliente, poblacion, direccion, numeroPedido, pedidos.codigoCliente, formaPago
  FROM clientes RIGHT JOIN pedidos ON clientes.codigoCliente = pedidos.codigoCliente
  WHERE poblacion = 'MADRID';

# CLIENTES SIN PEDIDOS
SELECT clientes.codigoCliente, poblacion, direccion, numeroPedido, pedidos.codigoCliente, formaPago
  FROM clientes LEFT JOIN pedidos ON clientes.codigoCliente = pedidos.codigoCliente
  WHERE poblacion = 'MADRID' AND pedidos.codigoCliente IS NULL;

