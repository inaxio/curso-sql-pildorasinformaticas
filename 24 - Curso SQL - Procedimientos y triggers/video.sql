/* 24 - Curso SQL - Procedimientos y triggers */

# Crear procedimiento
DELIMITER $$ #Delimitador de grupo
CREATE PROCEDURE calculaEdad(anioNacimiento INT)
  BEGIN
    DECLARE anioActual INT DEFAULT DATE_FORMAT(NOW(),'%Y');
    DECLARE edad int;
    SET edad = anioActual-anioNacimiento;
    SELECT edad;
  END;$$
DELIMITER ; # Reseter el delimitador a ;

# Llamar al procedimiento
CALL calculaEdad(1974);

# Consulta para conseguir el año actual.
SELECT DATE_FORMAT(NOW(),'%Y');

# Crear el trigger
DELIMITER $$
CREATE TRIGGER revisaPrecio_bu BEFORE UPDATE on productos
  FOR EACH ROW
  BEGIN
    if(NEW.precio < 0 ) THEN
      SET NEW.precio = 0;
    ELSEIF (NEW.precio>1000) THEN
      SET NEW.precio = 1000;
    END IF;
  END; $$
DELIMITER ;

# Borrar trigger
DROP TRIGGER IF EXISTS revisaPrecio_bu;

# Consulta de actualización
UPDATE productos SET precio = 75 WHERE codigoArticulo = 'AR01';
UPDATE productos SET precio = 8500 WHERE codigoArticulo = 'AR01';
UPDATE productos SET precio = -300 WHERE codigoArticulo = 'AR01';
UPDATE productos SET precio = 6.83 WHERE codigoArticulo = 'AR01';

# Crear el trigger (si no está entre 0 y 1000 no se cambia el precio).
DELIMITER $$
CREATE TRIGGER revisaPrecio_bu BEFORE UPDATE on productos
  FOR EACH ROW
  BEGIN
    if(NEW.precio < 0 ) THEN
      SET NEW.precio = OLD.precio;
    ELSEIF (NEW.precio>1000) THEN
      SET NEW.precio = OLD.precio;
    END IF;
  END; $$
DELIMITER ;
