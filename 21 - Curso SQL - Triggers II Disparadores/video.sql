/* 21 - Curso SQL - Triggers II Disparadores */

# Crear tabla
CREATE TABLE productosActualizados(
  anteriorCodigoArticulo VARCHAR(4),
  anteriorNombreArticulo VARCHAR(25),
  anteriorSeccion VARCHAR(15),
  anteriorPrecio DECIMAL(10,4),
  anteriorImportado VARCHAR(15),
  anteriorPaisOrigen VARCHAR(15),
  anteriorFecha DATE,
  nuevoCodigoArticulo VARCHAR(4),
  nuevoNombreArticulo VARCHAR(25),
  nuevoSeccion VARCHAR(15),
  nuevoPrecio DECIMAL(10,4),
  nuevoImportado VARCHAR(15),
  nuevoPaisOrigen VARCHAR(15),
  nuevoFecha DATE,
  usuario VARCHAR(15),
  fechaModificacion DATETIME
);

# Borrar tabla
DROP TABLE productosactualizados;

# Trigger
CREATE TRIGGER actualizarProductos_bu
BEFORE UPDATE ON productos
FOR EACH ROW
  INSERT INTO productosActualizados( anteriorCodigoArticulo, anteriorNombreArticulo, anteriorSeccion,
                                     anteriorPrecio, anteriorImportado, anteriorPaisOrigen, anteriorFecha,
                                     nuevoCodigoArticulo, nuevoNombreArticulo, nuevoSeccion, nuevoPrecio,
                                     nuevoImportado, nuevoPaisOrigen, nuevoFecha, usuario, fechaModificacion)
  VALUES ( OLD.codigoArticulo, OLD.nombreArticulo, OLD.seccion,
           OLD.precio, OLD.importado, OLD.paisOrigen, OLD.fecha,
           NEW.codigoArticulo, NEW.nombreArticulo, NEW.seccion, NEW.precio,
           NEW.importado, NEW.paisOrigen, NEW.fecha, CURRENT_USER(), now());

# Consulta de actualización
UPDATE productos SET precio = precio+20 WHERE codigoArticulo = 'AR07';
UPDATE productos SET precio = precio-20 WHERE codigoArticulo = 'AR07';























