/* 13 - Curso SQL - Consultas de acción II - Eliminación */

DELETE FROM clientes WHERE poblacion = 'MADRID';

# Tener en cuenta que la tabla PRODUCTOS está relacionada con PEDIDOS.
# Depende como este el tipo de actualización no permitirá borrar los productos.
# Ten en cuenta que estos están siendo usado en pedidos.
DELETE FROM productos WHERE seccion = 'DEPORTES' AND precio BETWEEN 50 AND 100;

# Ponemos DISTINCT para evitar repeticiones del campo que indiquemos.
SELECT DISTINCT empresa FROM clientes INNER JOIN pedidos ON clientes.codigoCliente = pedidos.codigoCliente;
# Ponemos DISTINCTROW para evitar repeticiones de filas, tiene en cuenta todos los campos.
SELECT DISTINCTROW empresa FROM clientes INNER JOIN pedidos ON clientes.codigoCliente = pedidos.codigoCliente;


# Access
DELETE DISTINCTROW CLIENTES.*, PEDIDOS.CÓDIGOCLIENTE
FROM CLIENTES LEFT JOIN PEDIDOS ON CLIENTES.CÓDIGOCLIENTE = PEDIDOS.CÓDIGOCLIENTE
WHERE PEDIDOS.CÓDIGOCLIENTE IS NULL;

# MySQL ( Simplificada sin DISTINCTROW )
SELECT * FROM clientes WHERE codigoCliente NOT IN ( SELECT DISTINCT codigoCliente FROM pedidos );



