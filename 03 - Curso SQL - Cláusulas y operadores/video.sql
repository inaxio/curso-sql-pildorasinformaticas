/* 03 - Curso SQL - Cláusulas y operadores */

SELECT nombreArticulo, seccion, precio FROM productos WHERE seccion = 'CERÁMICA';

SELECT nombreArticulo, seccion, precio FROM productos WHERE seccion = 'CERÁMICA' OR seccion = 'DEPORTES';

SELECT * FROM productos WHERE seccion = 'DE`PRTES' OR paisOrigen = 'USA';

SELECT * FROM productos WHERE precio > 300;

SELECT * FROM productos WHERE precio < 30;

SELECT * FROM productos WHERE fecha BETWEEN '2000-03-01' AND '2000-04-30';

SELECT * FROM productos WHERE fecha >= '2000-03-01' AND fecha <= '2000-04-30';

