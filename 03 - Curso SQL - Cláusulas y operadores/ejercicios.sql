/* 03 - Curso SQL - Cláusulas y operadores */

# 1. Realizar una consulta que muestre los campos “Empresa” y “Población” de la tabla “Clientes”.
SELECT empresa, poblacion FROM clientes;

# 2. Realizar una consulta que muestre los artículos de la sección “Cerámica”.
SELECT * FROM productos WHERE seccion = 'CERÁMICA';

# 3. Realizar una consulta que muestre los productos de la sección “Deportes” cuyo precio esté entre 100 y 200 €.
#    En la consulta solo se mostrarán los campos “Nombre de artículo” y “Precio”.
SELECT nombreArticulo, precio
FROM productos
WHERE seccion = 'DEPORTES'
      AND precio BETWEEN 100 AND 200;

# 4. Realizar una consulta que muestre los productos cuyo país no sea España.
SELECT *
FROM productos
WHERE paisOrigen = 'ESPAÑA';

# 5. Realizar una consulta que muestre los artículos españoles de la sección “Deportes”
#    o aquellos cuyo precio sea superior a 350 € independientemente de cual sea su sección o país de origen.
SELECT *
FROM productos
WHERE ( seccion= 'DEPORTES' AND paisOrigen = 'ESPAÑA')
  or precio > 350;

# 6. Realizar una consulta que muestre los productos cuya fecha esté entre 1/05/2001 y 15/12/2001.
#    En la consulta solo se visualizarán los campos “Nombre de artículo”, “Sección” y “Fecha”.
SELECT nombreArticulo, seccion, fecha
FROM productos
WHERE fecha BETWEEN '2001/05/01' AND '2001/12/15';
