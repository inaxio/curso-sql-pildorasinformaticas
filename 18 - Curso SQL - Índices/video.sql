/* 18 - Curso SQL - Índices */

# Crear tabla
# Access
CREATE TABLE ejemplo (
  dni TEXT,
  nombre TEXT,
  apellido TEXT,
  edad NUMBER,
  PRIMARY KEY (dni)
);
# MySQL

CREATE TABLE ejemplo (
  dni VARCHAR(20),
  nombre VARCHAR(20),
  apellido VARCHAR(20),
  edad TINYINT,
  PRIMARY KEY (dni)
);

# Modificar tabla para añadirle la clave primaria y un Índice de Clave Primaria
# Cada valor es único
# No permite NULL
ALTER TABLE ejemplo ADD PRIMARY KEY (dni);
ALTER TABLE ejemplo ADD PRIMARY KEY (nombre, apellido);

# Crear un Índice Ordinario
# Permite duplicados
# Permite NULL
CREATE INDEX miIndice ON ejemplo (apellido);
CREATE INDEX miIndice ON ejemplo (apellido);

# Crear un Índice Único
# No permite duplicados
# Permite NULL
CREATE UNIQUE INDEX miIndice ON ejemplo (apellido);

# Crear un Índice Compuesto
# Multiples columnas
# Permite NULL
CREATE UNIQUE INDEX miIndice ON ejemplo (nombre, apellido);