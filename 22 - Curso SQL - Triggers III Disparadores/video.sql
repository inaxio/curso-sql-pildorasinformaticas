/* 22 - Curso SQL - Triggers III Disparadores */

# Crear tabla
CREATE TABLE productosEliminados (
  codigoArticulo VARCHAR(5),
  nombreArticulo VARCHAR(15),
  seccion VARCHAR(15),
  precio DECIMAL(10,4),
  paisOrigen VARCHAR(15)
);


# Trigger
CREATE TRIGGER eliminarProductos_ad
  AFTER DELETE ON productos
  FOR EACH ROW
  INSERT INTO productosEliminados(codigoArticulo, nombreArticulo,
                                  seccion, precio, paisOrigen)
      VALUES (OLD.codigoArticulo, OLD.nombreArticulo,
              OLD.seccion, OLD.precio, OLD.paisOrigen);


# Borrar registro
DELETE FROM productos WHERE codigoArticulo = 'AR75';
DELETE FROM productos WHERE codigoArticulo = 'AR20';


# Agregar dos campos mas a la tabla productosEliminados
ALTER TABLE productosEliminados
  ADD COLUMN (usuario VARCHAR(20), fechaEliminacion DATETIME);


# Modificar el trigger
# Primero lo eliminamos
DROP TRIGGER IF EXISTS eliminarProductos_ad;
# Segundo lo volvemos a crear
# Trigger
CREATE TRIGGER eliminarProductos_ad
  AFTER DELETE ON productos
  FOR EACH ROW
  INSERT INTO productosEliminados(codigoArticulo, nombreArticulo,
                                  seccion, precio, paisOrigen,
                                  usuario, fechaEliminacion)
      VALUES (OLD.codigoArticulo, OLD.nombreArticulo,
              OLD.seccion, OLD.precio, OLD.paisOrigen,
              CURRENT_USER(), NOW());


# Borrar registro
DELETE FROM productos WHERE codigoArticulo = 'AR21';
DELETE FROM productos WHERE codigoArticulo = 'AR40';